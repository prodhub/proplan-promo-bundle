<?php
namespace ADW\ProplanPromoBundle\Services;

use Zent\VarsBundle\Entity\VarsManager;

/**
 * Class PromoService
 *
 * @package ADW\ProplanPromoBundle\Services
 */
class PromoService
{
    /** @var VarsManager */
    private $vars;
    /** @var array */
    private $config;

    public function __construct(VarsManager $vars, $config)
    {
        $this->vars = $vars;
        $this->config = $config;
    }

    public function getSystemName()
    {
        return $this->config['system_name'];
    }

    /**
     * @return int
     */
    private function getStartAt()
    {
        return strtotime($this->vars->getValue($this->config['vars']['date_start']));
    }

    /**
     * @return int
     */
    private function getEndAt()
    {
        return strtotime($this->vars->getValue($this->config['vars']['date_end']));
    }

    /**
     * @return bool
     */
    public function isStart()
    {
        return $this->getStartAt() <= time();
    }

    /**
     * @return bool
     */
    public function isEnd()
    {
        return $this->getEndAt() < time();
    }

    /**
     * @return bool
     */
    public function isAvailable()
    {
        return $this->isStart() && !$this->isEnd();
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }
}
