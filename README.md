#ADW Proplan Promo Bundle

### Текущий функционал:
- управление датами промо через админку

## Подключение
- Добавить с composer.json
````
"repositories": [
  { "type": "vcs", "url": "https://bitbucket.org/prodhub/proplan-promo-bundle.git" },
]        
````
- Вызвать
````
composer require adw/proplan-promo-bundle ~0.1
````

- Добавить в AppKernel
````
new ADW\ProplanPromoBundle\ADWProplanPromoBundle(),
new Zent\VarsBundle\ZentVarsBundle(),
````

- Создать сущность
````
// src/AppBundle/Entity/Vars.php
namespace AppBundle\Entity;

use ADW\ProplanPromoBundle\Validator\Period;
use Doctrine\ORM\Mapping as ORM;
use Zent\VarsBundle\Entity\BaseVars;

/**
 * @ORM\Entity()
 * @Period()
 */
class Vars extends BaseVars
{
}
````

- Добавить в config.yml
````
zent_vars:
    class: AppBundle\Entity\Vars
    
adw_proplan_promo:
  ailove_promo_repository: ailove.promo_repository # сервис для получения списка промо (с методом findAll)   
  promos:  
    urology:  # ключевое имя для промо
      system_name: "%ailove_partner_promo%" # название промо
      vars:
        date_start: "promo--urology- date-start" # названия переменных в entity Vars
        date_end: "proplan-urology-date-end"
    neurolgy:
      name: "%ailove_partner_promo%"
      vars: 
        date_start: "promo-neugoloy-date-start"
        date_end: "promo-neurology-date-end"
````
 ailove_promo_repository - сервис для получения списка промо (с методом findAll) 
 Нужно для ветов, так как там не используется ailove-bundle. Для проектов с ailove-bundle можно сделать будет отедельную версию (но пока так, лучше поддерживать одну версию).
 
- Обновить схему бд и добавить переменные vars proplan-promo-date-start и proplan-promo-date-end (в админке или через миграцию)

### Использование
````
$this->get('adw.proplan_promo.neurology')->isStart() // проверка начала промо Неврологии
$this->get('adw.proplan_promo.neurology')->isEnd() // проверка конца промо Неврологии
$this->get('adw.proplan_promo.urology')->isAvailable() // проверка идёт ли промо Урологии
````

#### Дополнительно можно добавить кешировать
````
doctrine_cache:
    providers:
        zent_vars_query_cache:
            type: apc # file_system, memcache or etc.
            namespace: query_cache_ns
            alias:
              - zent_vars.cache
````
