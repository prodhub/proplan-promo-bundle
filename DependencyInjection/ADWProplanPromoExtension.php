<?php
namespace ADW\ProplanPromoBundle\DependencyInjection;

use ADW\ProplanPromoBundle\Services\PromoService;
use ADW\ProplanPromoBundle\Validator\PeriodValidator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ADWProplanPromoExtension extends Extension
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config['promos'] as $name => $params) {
            $container->setDefinition('adw.proplan_promo.'.$name,
                new Definition(PromoService::class, [new Reference('zent_vars.manager'), $params]));
        }

        $promoPeriodValidator=new Definition(PeriodValidator::class, [
            new Reference('zent_vars.manager'),
            $config['promos'],
            new Reference($config['ailove_promo_repository'])
        ]);
        
        $promoPeriodValidator->addTag('validator.constraint_validator', ['alias'=>'proplan_promo_period']);
        $container->setDefinition('adw.proplan_promo.validation_period', $promoPeriodValidator);
        

        $loader = new Loader\YamlFileLoader($container,
            new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
