<?php
namespace ADW\ProplanPromoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_proplan_promo');
        $rootNode
            ->children()
                ->scalarNode('ailove_promo_repository')->isRequired()->end()
                ->arrayNode('promos')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('system_name')->isRequired()->end()
                            ->arrayNode('vars')
                                ->children()
                                    ->scalarNode('date_start')->isRequired()->end()
                                    ->scalarNode('date_end')->isRequired()->end()
                                ->end()
                        ->end()
                ->end()
            ->end()            ;

        return $treeBuilder;
    }

}
