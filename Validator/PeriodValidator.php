<?php
namespace ADW\ProplanPromoBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Zent\VarsBundle\Entity\VarsManager;

class PeriodValidator extends ConstraintValidator
{
    /** @var VarsManager */
    private $vars;
    /** @var array */
    private $config;

    private $promoRepo;

    /**
     * PeriodValidator constructor.
     *
     * @param VarsManager $varsManager
     * @param array       $config
     * @param             $promoRepository
     */
    public function __construct(VarsManager $varsManager, $config, $promoRepository)
    {
        $this->vars = $varsManager;
        $this->config = $config;
        $this->promoRepo = $promoRepository;
    }

    private function getPromoSystemName($name)
    {
        return $this->config[$name]['system_name'];
    }

    private function getPromoVarDateStart($name)
    {
        return $this->config[$name]['vars']['date_start'];
    }

    private function getPromoVarDateEnd($name)
    {
        return $this->config[$name]['vars']['date_end'];
    }

    public function validate($object, Constraint $constraint)
    {
        $varDatesPromo=[];
        $config = $this->config;
        foreach ($config as $name=> $params) {
            foreach ($params['vars'] as $var) {
                $varDatesPromo[$var] = $name;
            }
        }


        if (!isset($varDatesPromo[$object->getName()])) {
            return true;
        }
        $isFormat = preg_match("/^[\d]{2}.[\d]{2}.[\d]{4} [\d]{2}:[\d]{2}:[\d]{2}$/",
            $object->getValue());
        if (!$isFormat) {
            $this->addErrorForValue('Неверный формат даты (пример: 21.02.2017 23:59:59)');

            return false;
        }
        try {
            $date = new \DateTime($object->getValue());
        } catch (\Exception $e) {
            $this->addErrorForValue('Неверная дата (такой даты не существует)');

            return false;
        }

        $promoName = $varDatesPromo[$object->getName()];
        $ailovePromos = $this->promoRepo->findAll();
        $ailovePromoName=$this->getPromoSystemName($promoName);
        $promo = null;
        foreach ($ailovePromos as $ailovePromo) {
            if ($ailovePromo->getSystemName() == $ailovePromoName) {
                $promo = $ailovePromo;
                break;
            }
        }

        if (!$promo) {
            $this->addError('В ailove не надено промо: '.$ailovePromoName);

            return false;
        }

        $varDateStart = $this->getPromoVarDateStart($promoName);
        $varDateEnd = $this->getPromoVarDateEnd($promoName);
        /** @var \DateTime $ailoveDateStart */
        $ailoveDateStart = $promo->getDateBegin();
        $ailoveDateStart->setTime(0, 0, 0);
        /** @var \DateTime $ailoveDateEnd */
        $ailoveDateEnd = $promo->getDateEnd();
        $ailoveDateEnd->setTime(0, 0, 0);
        $isErrorDateStart = $object->getName() === $varDateStart && $date < $ailoveDateStart;
        $isErrorDateEnd = $object->getName() === $varDateEnd && $date > $ailoveDateEnd;
        if ($isErrorDateStart || $isErrorDateEnd) {
            $this->addErrorForValue(sprintf('Период промо в ailove с %s по %s',
                $ailoveDateStart->format('d.m.Y'), $ailoveDateEnd->format('d.m.Y')));

            return false;
        }
        if ($object->getName() === $varDateStart && $date >= new \DateTime($this->vars->getValue($varDateEnd))) {
            $this->addErrorForValue('Дата начала промо не должна быть больше даты окончания');

            return false;
        }
        if ($object->getName() === $varDateEnd && $date <= new \DateTime($this->vars->getValue($varDateStart))) {
            $this->addErrorForValue('Дата окончания промо не должна быть больше даты начала');

            return false;
        }

        return true;
    }

    private function addErrorForValue($msg)
    {
        $this->context->addViolationAt('value', $msg);
    }

    private function addError($msg)
    {
        $this->context->addViolation($msg);
    }
}
