<?php
namespace ADW\ProplanPromoBundle\Validator;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class Period extends Constraint
{
    public function validatedBy()
    {
        return 'proplan_promo_period';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
